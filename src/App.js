import "./App.css";
import Booking_Ticket from "./Booking_Tickets/Booking_Ticket";

function App() {
  return (
    <div className="App">
      <Booking_Ticket />
    </div>
  );
}

export default App;
