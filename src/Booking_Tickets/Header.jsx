import React, { Component } from "react";

export default class Header extends Component {
  render() {
    return (
      <div className="header">
        <h1>MOVIE SEAT SELECTION</h1>
      </div>
    );
  }
}
