import React, { Component } from "react";
import Header from "./Header";
import Content from "./Content";
import "./bookingTicket.css";
import Copy_Right from "./Copy_Right";

export default class Booking_Ticket extends Component {
  render() {
    return (
      <div className="bookTicKet">
        <Header />
        <Content />
        <Copy_Right />
      </div>
    );
  }
}
