import React, { Component } from "react";
import { connect } from "react-redux";
import { bookingTicketAction } from "./redux/actions/bookingTicketAction";

class Seat extends Component {
  renderTbody = () => {
    return this.props.dataSeat.map((listSeat, index) => {
      if (index === 0) {
        return (
          <tr key={index}>
            <td>{listSeat.hang}</td>
            {listSeat.danhSachGhe.map((seat, index) => {
              return <td key={index}>{seat.soGhe}</td>;
            })}
          </tr>
        );
      } else {
        return (
          <tr key={index}>
            <td>{listSeat.hang}</td>
            {listSeat.danhSachGhe.map((seat, index) => {
              let isDisabled = false;
              let cssReservedSeat = "";
              let cssSelectedSeat = "";
              if (seat.daDat) {
                isDisabled = true;
                cssReservedSeat = "reservedSeat";
              }
              let indexSelected = this.props.listSelectedSeat.findIndex(
                (item) => {
                  return seat.soGhe === item.soGhe;
                }
              );

              if (indexSelected !== -1) {
                cssSelectedSeat = "selectedSeat";
              }

              return (
                <td
                  className={`seats ${cssReservedSeat} ${cssSelectedSeat}`}
                  key={index}
                >
                  <button
                    disabled={isDisabled}
                    onClick={() => {
                      this.props.hanldeBookingTicket(seat);
                    }}
                  ></button>
                </td>
              );
            })}
          </tr>
        );
      }
    });
  };
  render() {
    return (
      <div className="seat">
        <h2>Please Select Your Seats</h2>
        <div className="seatAvailabilityList">
          <ul>
            <li className="greenBox">Selected Seat</li>
            <li className="redBox">Reserved Seat</li>
            <li className="emptyBox">Empty Seat</li>
          </ul>
        </div>
        <div className="seatStructure">
          <table>
            <tbody>{this.renderTbody()}</tbody>
          </table>
        </div>
        <div className="screen">
          <h2 className="wthree">Screen this way</h2>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    dataSeat: state.bookingTicketReducer.dataSeat,
    listSelectedSeat: state.bookingTicketReducer.listSelectedSeat,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    hanldeBookingTicket: (seat) => {
      dispatch(bookingTicketAction(seat));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Seat);
