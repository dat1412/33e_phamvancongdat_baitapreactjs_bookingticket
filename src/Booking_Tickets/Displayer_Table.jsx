import React, { Component } from "react";
import { connect } from "react-redux";
import { CANCEL_SEAT } from "./redux/constants/bookTicketConstant";
import { NumericFormat } from "react-number-format";
import { cancelSeatAction } from "./redux/actions/bookingTicketAction";

class Displayer_Table extends Component {
  renderTbody = () => {
    return this.props.listSelectedSeat.map((seat, index) => {
      return (
        <tr key={index}>
          <td>{seat.soGhe}</td>
          <td>
            <NumericFormat
              value={seat.gia}
              displayType={"text"}
              thousandSeparator={true}
              prefix={"₫"}
            />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleCancelSeat(seat.soGhe);
              }}
            >
              Cancel
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="displayerTable">
        <h2>Tickets Movie Bill</h2>
        <table>
          <thead>
            <tr>
              <th>Seats</th>
              <th>Price</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
          <tfoot>
            <tr>
              <th>Total Payment</th>
              <td>
                <NumericFormat
                  value={this.props.listSelectedSeat.reduce((total, seat) => {
                    return (total += seat.gia);
                  }, 0)}
                  displayType={"text"}
                  thousandSeparator={true}
                  prefix={"₫"}
                />
              </td>
            </tr>
          </tfoot>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    listSelectedSeat: state.bookingTicketReducer.listSelectedSeat,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleCancelSeat: (soGhe) => {
      dispatch(cancelSeatAction(soGhe));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Displayer_Table);
