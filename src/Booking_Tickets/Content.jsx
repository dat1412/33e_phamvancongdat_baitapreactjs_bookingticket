import React, { Component } from "react";
import Displayer_Table from "./Displayer_Table";
import Seat from "./Seat";

export default class Content extends Component {
  render() {
    return (
      <div className="content">
        <Seat />
        <Displayer_Table />
      </div>
    );
  }
}
