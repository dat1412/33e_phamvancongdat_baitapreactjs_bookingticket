import { BOOKING_TICKET, CANCEL_SEAT } from "../constants/bookTicketConstant";

export const bookingTicketAction = (seat) => {
  return {
    type: BOOKING_TICKET,
    payload: seat,
  };
};

export const cancelSeatAction = (soGhe) => {
  return {
    type: CANCEL_SEAT,
    payload: soGhe,
  };
};
