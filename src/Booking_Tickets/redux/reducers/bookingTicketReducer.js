import { dataSeat } from "../../dataSeat";
import { BOOKING_TICKET, CANCEL_SEAT } from "../constants/bookTicketConstant";

let initialState = {
  dataSeat: dataSeat,
  listSelectedSeat: [],
};

export let bookingTicketReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case BOOKING_TICKET: {
      let index = state.listSelectedSeat.findIndex((seat) => {
        return seat.soGhe === payload.soGhe;
      });
      let cloneListSelectedSeat = [...state.listSelectedSeat];

      if (index !== -1) {
        cloneListSelectedSeat.splice(index, 1);
      } else {
        cloneListSelectedSeat.push(payload);
      }

      state.listSelectedSeat = cloneListSelectedSeat;

      return { ...state };
    }
    case CANCEL_SEAT: {
      let index = state.listSelectedSeat.findIndex((seat) => {
        return seat.soGhe === payload;
      });
      let cloneListSelectedSeat = [...state.listSelectedSeat];

      if (index !== -1) {
        cloneListSelectedSeat.splice(index, 1);
      }

      state.listSelectedSeat = cloneListSelectedSeat;
      return { ...state };
    }
    default:
      return state;
  }
};
